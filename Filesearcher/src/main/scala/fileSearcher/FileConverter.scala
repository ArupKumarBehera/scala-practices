package fileSearcher

import java.io.File

/**
  * Created by Arup on 04-09-2016.
  */
object FileConverter {
  def convertToIOObject(file: File) =
    if(file.isDirectory()) DirectoryObject(file)
    else FileObject(file)

}
