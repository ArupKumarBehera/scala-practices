package fileSearcher

import java.io.File

trait IOObject {
  val file : File
  val name = file.getName()
}

//case class - Comes with its own companion object built in. No need to use val keyword in constr arg,
//as for case class it's by default public. Provide object decomposition and pattern matching

case class FileObject(file: File) extends IOObject

case class DirectoryObject(file: File) extends IOObject {

  def children() =
  try
    file.listFiles().toList map (file => FileConverter convertToIOObject file)
  catch {
    case _: NullPointerException => List()
  }

}