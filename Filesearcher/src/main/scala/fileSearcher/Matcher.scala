package fileSearcher

import java.io.File

/**
  * Created by Arup on 04-09-2016.
  * rootLocation :  Root location where search will be run. This can be a file or directory. If it's a file a simple filter check is run
  * else we need to check entire directory.Assumed that if it's not directory, then it must be a file.
  */
class Matcher(filter: String, val rootLocation: String = new File(".").getCanonicalPath()) {

  //convert root location into IOObject using Converter class
  val rootIOObject = FileConverter.convertToIOObject(new File(rootLocation))

  def execute() = {
    val matchedFiles = rootIOObject match {
      case file : FileObject if FilterChecker(filter) matches file.name => List(file)
      case directory : DirectoryObject => FilterChecker(filter) findMatchedFiles directory.children()
      case _ => List()
    }

    matchedFiles map(ioObject => ioObject.name)
  }

}
