
class MulTable(limit : Int){

  def generateTable(): Unit ={

    for(multiple <- 1 to limit) {
      for (multiplier <- 1 to 10) {
      println(multiple + " X " + multiplier + " = " + getMultiplication(multiple, multiplier))
    }
      println("---------------------------")
    }
  }

  def getMultiplication(multiple : Int,multiplier : Int) = multiple * multiplier

}
