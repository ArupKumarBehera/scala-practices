package calc

class MulTable(limit : Int){

  def generateTable(): Unit ={
    var multiple : Int = 1

    while(multiple <= limit){


      for(multiplier <- 1 to 10){
        println(multiple + " X " + multiplier + " = " + getMultiplication(multiple,multiplier))
      }

      println("---------------------------")

      multiple += 1
    }

  }

  def getMultiplication(multiple : Int,multiplier : Int) = multiple * multiplier

}

val limit : Int = args(0).toInt

val mulTable1 = new MulTable(limit)
mulTable1.generateTable()