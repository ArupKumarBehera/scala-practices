import java.net.{MalformedURLException, URL}


val url = try {
  new URL(args(0))
}
catch   {
    case e: MalformedURLException => {
      println("Invalid url !!. Setting url to : http://www.scala-lang.org")
      new URL("http://www.scala-lang.org")
    }
    case e: ArrayIndexOutOfBoundsException => new URL("http://www.scala-lang.org")
  }

println("Url : ")
println(url)