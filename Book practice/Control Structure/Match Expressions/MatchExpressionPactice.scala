val arg = if(args.isEmpty) 0 else args(0)

val choice =
  arg match {
  case "A" => "Apple"
  case "B" => "Ball"
  case "C" => "Cat"
  case "D" => "Dog"
  case "E" => "Elephant"
  case "F" => "Fog"
  case _ => "Huh ?"
}

println(choice)