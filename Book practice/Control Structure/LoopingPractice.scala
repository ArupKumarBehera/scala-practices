val filesHere = (new java.io.File("..\\")).listFiles

for(file <- filesHere){
  println(file)
}

//Filtering

println("---------------------------")
println("Scala Files : ")
//for(file <- filesHere; if(file.getName().endsWith(".scala"))){
//  println(file)
//}

//for
//{
//      file <- filesHere
//      if(file.getName().endsWith(".scala"))
//}
//println(file)

val scalaFiles = for {
  file <- filesHere
  if(file.getName().endsWith(".scala"))
}yield file

scalaFiles.foreach(println)