object MulTableApp {
  def main(args : Array[String]): Unit ={

    try {
      if(args.length == 0) {
        throw new IllegalArgumentException("Pass a number man !!")
      }
      else {
        println("Limit : " + args(0))
        new MulTable(args(0).toInt)
      }
    }
    catch {
      case ex : IllegalArgumentException  => println("Pass a number man !!")
    }
    finally{
      println("App Exit")
    }
  }
}

class MulTable(limit : Int) {


  //Approach 1

  //  def mulTable = for {
  //    i <- 1 to limit
  //    j <- 1 to 10
  //  } yield {
  //    i + " X " + j + " = " + i*j
  //
  //  }

  //mulTable.foreach(println)

  //Approach 2

  def multiTable = {
      val table = for (i <- 1 to limit) yield {
        val row = for (j <- 1 to 10) yield {
          val prod = (i * j).toString
          //String.format("%4s", Array(prod))
          i + " X " + j + " = " + i*j + '\n'
        }

        row.mkString + '\n'
      }

    table.mkString
  }

  println(multiTable)

}




