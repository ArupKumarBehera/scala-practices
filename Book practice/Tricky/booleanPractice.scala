// The logical-and and logical-or operations are short-circuited as in Java.
// Expressionsbuiltfrom theseoperatorsareonlyevaluatedasfarasneededto determine the result.
// In other words, the right hand side of logical-and and logical-orexpressionswon’tbeevaluatedifthelefthandsidedeterminesthe result

def salt() = { println("Salt"); false; }

def pepper() = { println("Pepper"); true; }

salt() && pepper()