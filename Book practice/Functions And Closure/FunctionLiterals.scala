//Concepts :

//1 - First-class functions a.k.a Function Literals
//2 - Function Values
//3 -

// At runtime, ﬁrst-class functions are represented by objects called function values

// Every function value is an instance of some class that extends one of several FunctionN traits in package scala,
// such as Function0 for functions with no parameters, Function1 for functions with one parameter,and soon.
// Each FunctionN trait has an apply method used to invoke the function

val res = (x : Int) => x + 1


println (res(if (args.length > 0) args(0).toInt else 0))

val increase = (x: Int) => {
  println("Line 1")
  println("Line 2")
  println("Line 3")
  x + 3
}

println(increase(if (args.length > 0) args(0).toInt else 0))

//Target Typing : The Scala compiler knows that x must be an integer
// ,because it sees that you are immediately using the function to ﬁlter a list of integers (referred to by someNumbers).
// This is called target typing

val someNumbers = List(-11, -10, -5, 0, 5, 10)

val divBy5 = someNumbers.filter((x)=> x % 5 == 0)
val divBy5WithoutBracket5 = someNumbers.filter(x=> x % 5 == 0)

println(divBy5)
println(divBy5WithoutBracket5)

//Place Holder

val divBy5PlaceHolder = someNumbers.filter(_ % 5 == 0)

println(divBy5PlaceHolder)

//Partially Applied Function
//You can also replace an entire parameter list with an underscore

divBy5.foreach(println _)






