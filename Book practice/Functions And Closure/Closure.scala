//more is a "free variable"
// x is a bound variable

var more = if(args.length > 1) args(1).toInt else 0

val addMore = ((x : Int) => x + more)

println(addMore(if(args.length > 0) args(0).toInt else 0))

//The function value (the object) addMore, that's created at runtime from this function literal is called a closure.

more = 33; //Changing value of more => Closure sees the change

println(addMore(if(args.length > 0) args(0).toInt else 0))

//Changes made to captured variable made by a closure is visible outsde the clsure

val someNumbers = List(-11,-10,-5,0,5,10)
var sum = 0
someNumbers.foreach(sum += _)

println(sum)

//sum is outside the scope of function literal. But the changes made to sum inside closure, was visible outside.