// A partially applied function is an expression in which you don’t
// supply all of the arguments needed by the function.
// Instead,you supply some,or none, of the needed arguments

def sum(a: Int,b: Int,c: Int) = a+b+c

println(sum(1,2,3))

val a = sum _

println(a(1,2,3))
// println(a(2))

val b = sum(1, _ : Int, 3)

println(b(4))

//If you are writing a partially applied function expression in which you
// leave off all parameters,such as println _or sum _,you can express it more
// concisely by leaving off the underscore if a function is required at that point in the code
//allowed only in places wherea function is required
// ,such as the invocation of foreach in this example

def printSquare(x : Int)  {
  print((x*x).toString() + " ")
}

def numbers = List(1,3,4,5,7,8)

numbers.foreach(printSquare _)
println("\n")
numbers.foreach(printSquare)