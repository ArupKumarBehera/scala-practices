//By concatinating * to the last argument in the parameter list of a function we can be able to pass variable number of params
//to the function. This function internally converts the arguments into array. You cannot directly pass an array, but there is a way
// yo can do that too.

def add(args : Int*) : Int = {
  var sum = 0;
  args.foreach(sum += _)
  sum
}

println(add(1,2,3,4,5))

// What if I directly pass array ?

val arr = Array(1,2,3,4,5)

//add(arr)  - Error occured

// Tweak :
println(add(arr : _*))