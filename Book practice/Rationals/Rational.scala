class Rational(n: Int,d: Int){

  private def gcd(a: Int,b: Int) : Int = {
    if(b==0) a else gcd(b,a%b)
  }

  private val num : Int = n/gcd(n,d)
  private val den : Int = d/gcd(n,d)

  //Auxilary Constructor
  def this(num : Int) = this(num,1)

  def +(that : Rational) : Rational =
    new Rational(this.num * that.den + that.num * this.den,this.den * that.den)

  def -(that: Rational) : Rational =
    new Rational(this.num * that.den - that.num * this.den,this.den * that.den)

  def *(that: Rational) : Rational =
    new Rational(this.num * that.num, this.den * that.den)

  def /(that : Rational) : Rational =
    new Rational(this.num * that.den,this.den * that.num)




  override def toString() = {
    num + "/" + den
  }



}

val r1 = new Rational(1,2)
val r2 = new Rational(3,4)

val res_add = r1+r2
val res_sub = r1-r2
val res_mul = r1*r2
val res_div = r1/r2

println(r1 + " + " + r2 + " = " + res_add)
println(r1 + " - " + r2 + " = " + res_sub)
println(r1 + " * " + r2 + " = " + res_mul)
println(r1 + " / " + r2 + " = " + res_div)

val r3 = new Rational(3)
println(r3)